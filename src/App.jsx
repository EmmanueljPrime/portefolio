import { useState } from "react";
import { NavBar } from "./NavBar/NavBar";
import {Profil} from "./Home/Profil"


function App() {

  return (
// 
      <div style={{ position: 'relative', minHeight: '100vh', overflow: 'hidden' }} >
        <div className="blobBackground"></div>
        <NavBar />
        <Profil />
      </div>

  )

}
export default App;