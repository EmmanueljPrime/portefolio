module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {

    extend: {
      colors: {
        'title-gradient-0': 'rgba(224, 223, 254, 1)',         // 0%
        'title-gradient-50': 'rgba(177, 169, 255, 1)',        // 50%
        'title-gradient-100': 'rgba(110, 106, 222, 1)',       // 100%
        'iris-11/50': '#b2a9ff7a',
        'iris-11': '#B1A9FF'
      },
      fontFamily: {
        'sintony': ['Sintony', 'sans-serif'],
        'poppins' : ['Poppins', 'sans-serif'],
      },
    },
  },
  plugins: [],
}

